<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProjectController extends AbstractController
{
    /**
     * @Route("/project/add", name="project")
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function add(Request $request, SluggerInterface $slugger)
    {
        $project = new Project();
        $project->setUser($this->getUser());
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imageFile  = $form->get('image')->getData();
            $file = $form->get('file')->getData();

            if ($imageFile && $file) {
                $originalImagename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                $safeFilename = $slugger->slug($originalFilename);
                $safeImagename = $slugger->slug($originalImagename);

                $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
                $newImagename = $safeImagename . '-' . uniqid() . '.' . $imageFile->guessExtension();

                try {
                    $imageFile->move(
                        $this->getParameter('projects_directory'),
                        $newFilename
                    );

                    $file->move(
                        $this->getParameter('projects_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    dump($e->getMessage());
                    die;
                }

                $project->setImage($newImagename);
                $project->setFile($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();
        }

        return $this->render('project/index.html.twig', [
            'projectForm' => $form->createView(),
        ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/projects", name="app_project_view")
     */
    public function index(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Project::class);
        $projects = $repository->findAll();
        return $this->render('project/view.html.twig', [
            'projects' => $projects
        ]);
    }
}
