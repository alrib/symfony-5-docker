# Symfony 5 docker containers

A Proof-of-concept of Symfony 5

```
git clone git@gitlab.com:alrib/symfony-5-docker.git

cd symfony-5-docker

cd docker

docker-compose up -d
```

### PHP (PHP-FPM)

Composer is included

```
docker-compose run php-fpm composer 
```